# edot
Minimal Minetest subgame. Useful as test envinronment or sample for new subgame creators.

## License
MIT

## Media files

Files from Minetest Game (some renamed) 
* edot_nodes_dummyblock_dig.ogg
* edot_nodes_dummyblock_dug_node.1.ogg
* edot_nodes_dummyblock_dug_node.2.ogg
* edot_nodes_dummyblock_footstep.1.ogg
* edot_nodes_dummyblock_footstep.2.ogg
* edot_nodes_dummyblock_place_node.ogg
* crack_anylength.png
* bubble.png
* heart.png
* player.png
* player_back.png
* wieldhand.png

Files based on Minetest Game but modified / remixed
* icon.png

Files created by xeranas

* edot_nodes_dummyblock.png